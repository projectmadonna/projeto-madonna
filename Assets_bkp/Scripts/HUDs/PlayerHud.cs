﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHud : MonoBehaviour {
<<<<<<< HEAD
    [Header("ASSIGN:")]
    public GameObject pauseMenuUI;
    public GameObject keepPlaying_confirmationPanel;

    [Tooltip("The main UI image shown when there is a special item in the level")]
    public GameObject specialItemUI;
    [Tooltip("The check icon of the special item, shown when this mission is completed")]
    public GameObject specialItemCheckUI;
    [Tooltip("The main UI image shown when there are frozen rocks in the level")]
    public GameObject frozenRocksUI;
    [Tooltip("The check icon of the frozen rocks, shown when this mission is completed")]
    public GameObject frozenRocksCheckUI;
    [Tooltip("The main UI image shown when there are breakable tiles in the level")]
    public GameObject breakableTilesUI;
    [Tooltip("The check icon of the breakable tiles, shown when this mission is completed")]
    public GameObject breakableTilesCheckUI;

    public static Text heartsAvailable;

    [HideInInspector]
    public GameGrid grid;

    private Match3Manager match3Manager;
=======

    public GameObject pauseMenuUI;
    public static Text heartsAvailable;
    public GameGrid grid;
    private Match3Manager match3Manager;
    public GameObject keepPlaying_confirmationPanel;
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9

    private void Start() {
        grid = GameObject.FindWithTag("Grid").GetComponent<GameGrid>();
        match3Manager = grid.match3Manager;
<<<<<<< HEAD
=======

        if (heartsAvailable != null) {
            if(PlayerPersistence.HasFileCreated()) 
                heartsAvailable.text = FormatNumber(PlayerPersistence.GetHeartsStored());
        }
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9
    }

    public void PauseGame() {
        if (!GameManager.isGamePaused) {
            if(pauseMenuUI != null)
                pauseMenuUI.SetActive(true);
            GameManager.PauseGame();
        }
        SoundManagerScript.PlayClickButton();
    }

    //
    // ----------------- POWER UPS --------------------------------------------//
    //

<<<<<<< HEAD
    public void ShuffleGrid(Button button) { 
=======
    public void ShuffleGrid(Button button) {
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9
        if (match3Manager.activePowerUp == PowerUp.None) {
            //If the Shuffle power up hasn't been used
            if (!match3Manager.powerUpsUsed.Contains(PowerUp.Shuffle)) {
                match3Manager.activePowerUp = PowerUp.Shuffle;

                //Shufle the grid
                grid.ShuffleGrid();

                //Sets the button's color to be dark
                button.GetComponent<Image>().color = new Color32(80, 80, 80, 255);

                //Adds this powerup to the list of used powerups
                match3Manager.powerUpsUsed.Add(PowerUp.Shuffle);

                Invoke("ClearPowerUpSelection", 0.2f);
            } else {
                // TODO: tell the user that he has already used the Shuffle power up
            }
            SoundManagerScript.PlayClickButton();
        }
    }

    public void Pick(Button button) {
        if (match3Manager.activePowerUp == PowerUp.None) {
            //If the Pick power up hasn't been used
            if (!match3Manager.powerUpsUsed.Contains(PowerUp.Pick)) {
                match3Manager.activePowerUp = PowerUp.Pick;

                //Sets the button's color to be dark
                button.GetComponent<Image>().color = new Color32(80, 80, 80, 255);

                //Adds this powerup to the list of used powerups
                match3Manager.powerUpsUsed.Add(PowerUp.Pick);

                //ClearPowerUpSelectionCoRoutine();
            } else {
                // TODO: tell the user that he has already used the Pick power up
            }
            SoundManagerScript.PlayClickButton();
        }
    }

    public void Chamma(Button button) {
        if (match3Manager.activePowerUp == PowerUp.None) {
            //If the Chamma power up hasn't been used
            if (!match3Manager.powerUpsUsed.Contains(PowerUp.Chamma)) {
                match3Manager.activePowerUp = PowerUp.Chamma;
                match3Manager.IncreaseMovesLeft(5);

                //Sets the button's color to be dark
                button.GetComponent<Image>().color = new Color32(80, 80, 80, 255);

                //Adds this powerup to the list of used powerups
                match3Manager.powerUpsUsed.Add(PowerUp.Chamma);

                Invoke("ClearPowerUpSelection", 0.2f);
            } else {
                // TODO: tell the user that he has already used the Chamma power up
            }
            SoundManagerScript.PlayClickButton();
        }
    }

    public void Dynamite(Button button) {
        if (match3Manager.activePowerUp == PowerUp.None) {
            //If the Dynamite power up hasn't been used
            if (!match3Manager.powerUpsUsed.Contains(PowerUp.Dynamite)) {
                match3Manager.activePowerUp = PowerUp.Dynamite;

                //Sets the button's color to be dark
                button.GetComponent<Image>().color = new Color32(80, 80, 80, 255);

                //Adds this powerup to the list of used powerups
                match3Manager.powerUpsUsed.Add(PowerUp.Dynamite);

                //ClearPowerUpSelectionCoRoutine();
            } else {
                // TODO: tell the user that he has already used the Dynamite power up OR leave it with no feedback?
            }
            SoundManagerScript.PlayClickButton();
        }
    }

    private void ClearPowerUpSelection() {
        match3Manager.activePowerUp = PowerUp.None;
    }

    //Treats the format of the number set to the UI Text element (< 10)
    private string FormatNumber(int number) {
        if (number < 10) {
            return "0" + number.ToString();
        } else {
            return number.ToString();
        }
    }

    //Keep playing - Confirmation Panel

<<<<<<< HEAD
    public void ShowKeepPlayingConfirmation() {
        //SoundManagerScript.PlayClickButton();
        if (keepPlaying_confirmationPanel != null) {
            StartCoroutine(ShowKeepPlayingConfirmationCo());
        }
    }

    public IEnumerator ShowKeepPlayingConfirmationCo() {
        yield return new WaitForSeconds(1f);
        if (!keepPlaying_confirmationPanel.activeInHierarchy) {
            keepPlaying_confirmationPanel.SetActive(true);
            gameObject.GetComponent<Animator>().Play("KeepPlayingConfirmShowUp");
        }
=======
    public void ConfirmKeepPlaying() {
        SoundManagerScript.PlayClickButton();
        keepPlaying_confirmationPanel.SetActive(true);
        gameObject.GetComponent<Animator>().Play("ExitConfirmationShowUp");
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9
    }

    public void KeepPlayingLevel() {
        if (keepPlaying_confirmationPanel != null) {
            //Hide panel
            keepPlaying_confirmationPanel.SetActive(false);
            grid.match3Manager.keepPlayingLevel = true;
        }
    }

    public void SkipPlayingLevel() {
<<<<<<< HEAD
        //gameObject.GetComponent<Animator>().Play("KeepPlayingConfirmHide");
        StartCoroutine(ShowKeepPlayingConfirmationCo());
=======
        gameObject.GetComponent<Animator>().Play("ExitConfirmationHide");
        StartCoroutine(SkipPlayingConfirmationCo());
    }

    public IEnumerator SkipPlayingConfirmationCo() {
        yield return new WaitForSeconds(.25f);
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9
        keepPlaying_confirmationPanel.SetActive(false);
        grid.match3Manager.CallVictoryRoutines(3);
    }
}
