﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour {
    [SerializeField, Tooltip("Select the name of this level. DO NOT select the quiz level (ending with Q). If you can't find it, you might have forgotten to put this level's name in EnumCollection.Levels.")]
    private Levels sceneName;
    [Header("ASSIGN:")]
    public Text scoreText;
    public GameObject star0_active;
    public GameObject star1_active;
    public GameObject star2_active;

    private void OnBecameVisible() {
        SetLevelButtons();
    }

    private void Start() {
        //If the LevelsPlayed List<> doesn't exist in the PlayerData stored
<<<<<<< HEAD
        //if (PlayerPersistence.LoadPlayerData().levelsPlayed != null) {
            //Debug.Log("LevelsPlayed in file: " + PlayerPersistence.LoadPlayerData().levelsPlayed);
        //} else {
=======
        if (PlayerPersistence.LoadPlayerData().levelsPlayed != null) {
            Debug.Log("LevelsPlayed in file: " + PlayerPersistence.LoadPlayerData().levelsPlayed);
        } else {
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9
            //If the player hasn't played yet, create a LevelsPlayed List<>, add the first level in it and save it to the local file
            //LevelInfo firstLevelInfo = new LevelInfo();
            //firstLevelInfo.name = SceneManager.GetActiveScene().name + "L1";
            //firstLevelInfo.score = 0;
            //firstLevelInfo.stars = 0;
            //firstLevelInfo.world = int.Parse(SceneManager.GetActiveScene().name.Substring(1,1));
            //firstLevelInfo.number = 1;

            //PlayerPersistence.AddLevelToFile(firstLevelInfo);
                        
            //Debug.Log("Saved LevelInfo to file: " + PlayerPersistence.LoadPlayerData().LevelsPlayed);
<<<<<<< HEAD
        //}
=======
        }
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9

        SetLevelButtons();
    }

    

    public void SetLevelButtons() {
        if (PlayerPersistence.HasFileCreated()) {
            //Debug.Log("currentButton = " + sceneName);
            //Trying to retrieve the level info from the PlayerData, if available
            LevelInfo levelInfo = (LevelInfo)PlayerPersistence.HasPlayedLevel(sceneName.ToString());

            //-----------LEVEL FOUND in PlayerData
            //
<<<<<<< HEAD
            //Debug.Log("------------------- Inicio LevelSelection.Start()");
            if (levelInfo != null) {
                //Debug.Log("level " + sceneName + " FOUND!");
                //Debug.Log("level (" + levelInfo.name + ") stars = " + levelInfo.stars);
=======
            Debug.Log("------------------- Inicio LevelSelection.Start()");
            if (levelInfo != null) {
                Debug.Log("level " + sceneName + " FOUND!");
                Debug.Log("level (" + levelInfo.name + ") stars = " + levelInfo.stars);
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9
                //Setting-Activating the stars
                switch (levelInfo.stars) {
                    case 1:
                    if (star0_active != null) {
                        star0_active.SetActive(true);
                    } else {
                        Debug.LogError("Variable star0_active nor assigned!");
                    }
                    break;
                    case 2:
                    if (star0_active != null) {
                        star0_active.SetActive(true);
                    } else {
                        Debug.LogError("Variable star0_active nor assigned!");
                    }
                    if (star1_active != null) {
                        star1_active.SetActive(true);
                    } else {
                        Debug.LogError("Variable star1_active nor assigned!");
                    }
                    break;
                    case 3:
                    if (star0_active != null) {
                        star0_active.SetActive(true);
                    } else {
                        Debug.LogError("Variable star0_active nor assigned!");
                    }
                    if (star1_active != null) {
                        star1_active.SetActive(true);
                    } else {
                        Debug.LogError("Variable star1_active nor assigned!");
                    }
                    if (star2_active != null) {
                        star2_active.SetActive(true);
                    } else {
                        Debug.LogError("Variable star2_active nor assigned!");
                    }
                    break;
                }

                scoreText.text = levelInfo.score.ToString();

                ActivateButton();

                //-----------LEVEL NOT FOUND in PlayerData
                //
            } else {
<<<<<<< HEAD
                //Debug.Log("current level " + sceneName + " NOT FOUND!");
                int currentLevelNumber = int.Parse(sceneName.ToString().Substring(3, 1));
                //Debug.Log("current level number in button = " + currentLevelNumber);
=======
                Debug.Log("current level " + sceneName + " NOT FOUND!");
                int currentLevelNumber = int.Parse(sceneName.ToString().Substring(3, 1));
                Debug.Log("current level number in button = " + currentLevelNumber);
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9

                //If it's the first level of the world, then make interactible
                if (currentLevelNumber <= 1) {
                    ActivateButton();

                    //If it's not the first level of the world
                } else {
                    //Gets the number of the last level
                    int priorLevelNumber = int.Parse(sceneName.ToString().Substring(3, 1)) - 1;
<<<<<<< HEAD
                    //Debug.Log("previous level number = " + priorLevelNumber);
                    //Mounting last level's name
                    string priorLevelName = sceneName.ToString().Substring(0, 3) + priorLevelNumber.ToString();
                    //Debug.Log("previous level name = " + priorLevelName);
=======
                    Debug.Log("previous level number = " + priorLevelNumber);
                    //Mounting last level's name
                    string priorLevelName = sceneName.ToString().Substring(0, 3) + priorLevelNumber.ToString();
                    Debug.Log("previous level name = " + priorLevelName);
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9

                    //Check if the last level is stored
                    LevelInfo lastLevelInfo = (LevelInfo)PlayerPersistence.HasPlayedLevel(priorLevelName);
                    //If we find the last level stored in PlayerData, Activate this button
                    if (lastLevelInfo != null) {
<<<<<<< HEAD
                        //Debug.Log("previous level FOUND");
                        ActivateButton();
                    } else {
                        //Debug.Log("previous level NOT FOUND");
=======
                        Debug.Log("previous level FOUND");
                        ActivateButton();
                    } else {
                        Debug.Log("previous level NOT FOUND");
>>>>>>> 84174d6109dad64f39a6d0b5206abd7519e668a9
                        DeactivateButton();
                    }
                }
            }
        } else {
            int currentLevelNumber = int.Parse(sceneName.ToString().Substring(3, 1));
            //Debug.Log("currentLevelNumber = " + currentLevelNumber);
            //If it's the first level of the world, then make interactible
            if (currentLevelNumber <= 1) {
                ActivateButton();
                //If it's not the first level of the world
            }
        }
        
    }    

    public void loadLevel() {
        SoundManagerScript.PlayClickButton();
        GameManager.LoadLevel(sceneName.ToString());
    }

    public void loadWorldSelection() {
        GameManager.LoadLevel("WorldSelection");
    }

    public void loadMainMenu() {
        GameManager.LoadLevel("MainMenu");
    }

    public void ActivateButton() {
        gameObject.GetComponent<Button>().interactable = true;
    }

    public void DeactivateButton() {
        gameObject.GetComponent<Button>().interactable = false;
    }
}
