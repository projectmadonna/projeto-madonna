﻿#if UNITY_ANDROID
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;

public class FirebaseDatabaseManager : MonoBehaviour {

    private static DatabaseReference dbReference;
    
    public const string CONTEXT_RELATED_DB_REF = "ContextRelated";
    public const string GENERIC_COUNTERS_DB_REF = "GenericCounters";
    public const string GAME_LAUNCHES_DB_REF = "GameLaunches";
    public const string BYDATE_DB_REF = "ByDate";
    public const string BYTIME_DB_REF = "ByTime";
    public const string MUSIC_MUTES_DB_REF = "MusicMutes";
    public const string MUSIC_UNMUTES_DB_REF = "MusicUnmutes";
    public const string SFX_MUTES_DB_REF = "SfxMutes";
    public const string SFX_UNMUTES_DB_REF = "SfxUnmutes";
    public const string COMPLETIONS_DB_REF = "Completions";
    public const string GAMEOVERS_DB_REF = "GameOvers";
    public const string LEVELS_DB_REF = "Levels";
    public const string TOTAL_DB_REF = "Total";

    // Start is called before the first frame update
    void Start() {
        //FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            //var dependencyStatus = task.Result;
            //if (dependencyStatus == Firebase.DependencyStatus.Available) {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                //   app = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
                //Debug.Log("Firebase available.");
                //FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://rokit-b819e.firebaseio.com/");
                // Get the root reference location of the database.
                //dbReference = FirebaseDatabase.DefaultInstance.RootReference;

                //AddGameLaunch();
            //} else {
                //Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            //}
        //});

    }
    class ClassTest {
        public string name;
        public string value;

        public ClassTest(string name, string value) {
            this.name = name;
            this.value = value;
        }
    }

    public static void AddGameLaunch() {
        
        string total = JsonUtility.ToJson(new ClassTest("Total","1"));
        dbReference.Child("GenericCounters").Child("GameLaunches").Child("Total").Child("kk").SetRawJsonValueAsync(total);
        //try {
        //dbInstance.GetReference(GENERIC_COUNTERS_DB_REF).Child(GAME_LAUNCHES_DB_REF).Child(TOTAL_DB_REF).GetValueAsync().ContinueWith(task => {
        //    if (task.IsFaulted) {
        //        // Handle the error...
        //    } else if (task.IsCompleted) {
        //        DataSnapshot snapshot = task.Result;
        //        // Do something with snapshot...
        //        if (snapshot.Exists) {
        //            long totalGameLaunches = (long)snapshot.Value;
        //            Debug.Log("totalGameLaunches = " + totalGameLaunches);
        //        } else {
        //            Debug.Log("snapshot doesn't exist");
        //        }
        //    }
        //});
        //} catch (NullReferenceException e) {
        //    Debug.LogError("NullReferenceException in the database reference.");
        //}
    }

}

#endif
