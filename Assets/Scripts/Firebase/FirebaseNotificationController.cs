﻿#if UNITY_ANDROID
using System.Collections;
using System.Collections.Generic;
using Firebase;
using UnityEngine;

public class FirebaseNotificationController : MonoBehaviour
{
    private DependencyStatus dependencyStatus;

    // Start is called before the first frame update
    void Start() {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available) {
                InitializeFirebaseMessaging();
            } else {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void InitializeFirebaseMessaging() {
        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
        //Firebase.Messaging.FirebaseMessaging.Send

        //Playing the game for the first time (so there won't be a playerData file created)
        if (!PlayerPersistence.HasFileCreated()) {
            Firebase.Messaging.FirebaseMessaging.SubscribeAsync("/topics/all");
        }
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token) {
        Debug.Log("OnTokenReceived Registration Token: " + token.Token);
        
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e) {
        Debug.Log("Received a new message from: " + e.Message.From);

        var notification = e.Message.Notification;

        if (notification != null) {
            Debug.Log("title: " + notification.Title);
            Debug.Log("body: " + notification.Body);
        }
        if (e.Message.From.Length > 0)
            Debug.Log("from: " + e.Message.From);
        if (e.Message.Link != null) {
            Debug.Log("link: " + e.Message.Link.ToString());
        }
        if (e.Message.Data.Count > 0) {
            Debug.Log("data:");
            foreach (System.Collections.Generic.KeyValuePair<string, string> iter in
                     e.Message.Data) {
                Debug.Log("  " + iter.Key + ": " + iter.Value);
            }
        }
    }
}
#endif